/**
 * @file
 * Includes js for honeybadger_js.
 */

(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.honeybadgerJS = {
    attach: function (context, settings) {

      Honeybadger.configure({
        apiKey: drupalSettings.honeybadgerJS.apiKey,
        environment: drupalSettings.honeybadgerJS.environment,
      });

    }
  }

})(jQuery, Drupal, drupalSettings);
