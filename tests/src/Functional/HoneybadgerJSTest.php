<?php

namespace Drupal\Tests\honeybadger_js\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the Honeybadger JS configuration.
 *
 * @group honeybadger_js
 */
class HoneybadgerJSTest extends BrowserTestBase {

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'user',
    'honeybadger_js',
  ];

  /**
   * Tests the Honeybadger JS configuration.
   */
  public function testConfiguration() {
    $test_api_key = '12345';
    $test_environment = 'test';

    $admin_user = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($admin_user);
    $session = $this->assertSession();

    $settings_form_path = Url::fromRoute('honeybadger_js.admin_settings_form');
    $this->drupalGet($settings_form_path);
    $session->statusCodeEquals(200);

    $enable_honeybadger = $session->fieldExists('enable_honeybadger')->getValue();
    $this->assertTrue($enable_honeybadger == FALSE);
    $api_key = $session->fieldExists('api_key')->getValue();
    $this->assertTrue($api_key === '');
    $environment = $session->fieldExists('environment')->getValue();
    $this->assertTrue($environment === '');

    $updated_values = [
      'enable_honeybadger' => '1',
      'api_key' => $test_api_key,
      'environment' => $test_environment,
    ];
    $this->drupalPostForm($settings_form_path, $updated_values, 'Save configuration');

    $this->drupalGet($settings_form_path);
    $enable_honeybadger = $session->fieldExists('enable_honeybadger')->getValue();
    $this->assertTrue($enable_honeybadger == TRUE);
    $api_key = $session->fieldExists('api_key')->getValue();
    $this->assertTrue($api_key === $test_api_key);
    $environment = $session->fieldExists('environment')->getValue();
    $this->assertTrue($environment === $test_environment);
    $this->drupalLogout();

    $authenticated_user = $this->drupalCreateUser([
      'access content',
    ]);
    $this->drupalLogin($authenticated_user);
    $this->drupalGet('<front>');
    $drupal_settings_json = $this->getSession()
      ->getPage()
      ->find('xpath', '//script[@data-drupal-selector="drupal-settings-json"]')
      ->getText();
    $drupal_settings = json_decode($drupal_settings_json);
    $this->assertTrue($drupal_settings->honeybadgerJS->apiKey === $test_api_key);
    $this->assertTrue($drupal_settings->honeybadgerJS->environment === $test_environment);
  }

}
