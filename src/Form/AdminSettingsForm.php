<?php

namespace Drupal\honeybadger_js\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures Honeybadger JS module settings.
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'honeybadger_js_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'honeybadger_js.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('honeybadger_js.settings');
    $form['enable_honeybadger'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Honeybadger'),
      '#description' => $this->t('Click ON to activate the connection with Honeybadger.'),
      '#default_value' => $config->get('enable_honeybadger'),
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
    ];
    $form['environment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment'),
      '#default_value' => $config->get('environment'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('honeybadger_js.settings')
      ->set('enable_honeybadger', $form_state->getValue('enable_honeybadger'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('environment', $form_state->getValue('environment'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
